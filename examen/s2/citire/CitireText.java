package examen;

import java.awt.event.*
import javax.swing.*;
import java.io.*;

public class CitireText extends JFrame
{
	JTextField textFis;
	JButton Buton;
	
	CitireText()
	{
	SetTitle("Aplicatie text");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400,400);
        setVisible(true);
	}
	
	public void init()
    	{
        this.setLayout(null);

        textFis = new JTextField();
        textFis.setBounds(10,50,80,20);

        Buton = new JButton("OK");
        Buton.setBounds(10,50,50,20);
        Buton.addActionListener(new TratareButon());
        
        add(textFis);
        add(Buton);
    	}

	public static void main(String[] args)
	{
		CitireText citire = new CitireText();
	}
}
